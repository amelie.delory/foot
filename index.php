

<!doctype html>

<html>
<head>
<link rel="stylesheet" href="style.css" media="screen" title="no title" charset="utf-8">
<meta charset="utf-8">
<title>Football</title>
</head>
<body>

  <div class="titre">
    <h1> Base de donnée - équipes et joueurs de Foot </h1>
  </div>
  <div class="teams">

  <h2>Les équipes</h2>
   <?php

   #ini_set("display_errors","true");

   $handle=mysqli_connect("localhost","root","1234","foot");

   if($handle) {

     //echo "Connexion OK"."<br>";

   }

   else {

     echo "Connexion HS"."<br>";

     echo mysqli_connect_error();

   }

   $query="SELECT * FROM teams";

   $result=mysqli_query($handle,$query);

   echo "<ul>";

   while($line=mysqli_fetch_array($result)) {
     echo "<li>"."[".$line["id"]."] ".$line["country"]." ".$line["nickname"];
     echo "<a href=\"delete_team.php?id=". $line["id"] . "\"> [ X ] </a>" ;
     echo "<a href=\"update_country.php?id=". $line["id"] . "\"> UPDATE </a>" . "</li>" ;

   }

   echo "</ul>";

   if($result) {
    // echo "Résultat OK"."<br>";
   }

   else {
     echo "Résultat HS"."<br>";
     echo mysqli_error($handle);
   }

 ?>


<h3>Ajouter pays - surnom</h3>

<form action="create_team.php" method="post">

<label for="country">Pays</label>
<input class="champ" type="text" name="country" placeholder="Country..."><br>
<label for="nickname">Surnom</label>
<input class="champ" type="text" name="nickname" placeholder="Nickname..."><br>
<input type="submit" value="Ajouter">

</form>
</div>

<div class="players">

<h2>Les joueurs</h2>
<?php


$query="SELECT * FROM players";

$result=mysqli_query($handle,$query);

echo "<ul>";

while($line=mysqli_fetch_array($result)) {
  echo "<li>"."[".$line["id"]."] ".$line["name"]." ".$line["first_name"]." ".$line["birth"]." ".$line["id_team"];
  echo "<a href=\"delete_player.php?id=". $line["id"] . "\"> [ X ] </a>" ;
  echo "<a href=\"update_player.php?id=". $line["id"] . "\"> UPDATE </a>" . "</li>" ;

}

echo "</ul>";

?>

<h3>Ajouter nom - prénom - date de naissance</h3>

<form action="create_player.php" method="post">

<label for="name">Nom</label>
<input class="champ" type="text" name="name" placeholder="Name..."><br>

<label for="first_name">Prénom</label>
<input class="champ" type="text" name="first_name" placeholder="First name..."><br>

<label for="birth">Date de naissance</label>
<input class="champ" type="date" name="birth" placeholder="Birth..."><br>

<input type="submit" value="Ajouter">

</form>
</div>

</body>
</html>
